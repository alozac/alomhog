package fr.umlv.orthopro;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.umlv.orthopro.entities.Dictation;
import fr.umlv.orthopro.entities.Rule;
import fr.umlv.orthopro.entities.Sentence;

public class RuleTest extends JPAHibernateTest{
	
	@Test
	public void newRuleFieldsNull() {
		em.getTransaction().begin();
		assertAll(
				() -> assertThrows(PersistenceException.class,
								   () -> em.persist(new Rule("name", null, "détails"))),
				() -> assertThrows(PersistenceException.class,
						   		   () -> em.persist(new Rule("name", "indice", null)))
		);
		em.getTransaction().rollback();
	}
	
	@Test
	@BeforeAll
	public static void testPersist() {
		em.getTransaction().begin();
		
		Rule s = new Rule("nom regle", "indice", "corps regle");
		em.persist(s);
		
		em.getTransaction().commit();

		List<Rule> sent = em.createNamedQuery("Rule.getAll", Rule.class).getResultList();
		assertNotNull(sent);
		assertEquals(1, sent.size());
	}
	

	@Test
	public void testUpdate() {
		em.getTransaction().begin();
		
		Rule rule = em.find(Rule.class, "nom regle");
		
		rule = new Rule("nom regle", "to merge", "to merge");

		em.merge(rule);

		em.getTransaction().commit();

		Rule rule2 = em.find(Rule.class, rule.getName());
		assertEquals(rule2.getHint(), "to merge");
	}
	
	
	@Test
	public void testDelete() {
		em.getTransaction().begin();
		
		List<Rule> before = em.createNamedQuery("Rule.getAll", Rule.class).getResultList();

		em.remove(before.get(0));
		em.getTransaction().commit();
		
		List<Rule> after = em.createNamedQuery("Rule.getAll", Rule.class).getResultList();
		
		assertTrue(before.size() == after.size()+1);
	}
	
	
	@Test
	public void testAddSentences() {
		em.getTransaction().begin();
		Rule rule = new Rule("nom regle", "indice", "corps regle");
		for (int i = 0 ; i < 15 ; i++) {
			rule.addSentence(new Sentence("phrase " + i, "correction " + i));
		}
		
		em.persist(rule);
		em.getTransaction().commit();
		
		Rule rule2 = em.find(Rule.class, "nom regle");
		
		assertEquals(rule2.getSentences().size(), 15);
	}
	@Test
	public void testName() {
		em.getTransaction().begin();
		Rule r = new Rule("rule0", "indice", "corps");
		em.persist(r);
		em.getTransaction().commit();
		assertEquals("rule0", em.find(Rule.class, r.getName()).getName());
	}
	
	@Test
	public void testHint() {
		em.getTransaction().begin();
		Rule r = new Rule("rule1","indice","corps");
		em.persist(r);
		r.setHint("indice1");
		em.merge(r);
		em.getTransaction().commit();
		assertEquals("indice1", em.find(Rule.class, r.getName()).getHint());
	}
	@Test
	public void testBody() {
		em.getTransaction().begin();
		Rule r = new Rule ("rule2", "indice","corps");
		em.persist(r);
		r.setBody("corps1");
		em.merge(r);
		em.getTransaction().commit();
		assertEquals("corps1", em.find(Rule.class, r.getName()).getBody());
	}
	
	@Test
	public void testDictation() {
		em.getTransaction().begin();
		Rule r = new Rule("rule3","indice","corps");
		Dictation d = new Dictation("name0", "diff","descr","audio",r);
		r.setDictation(d);
		em.persist(r);
		em.persist(d);
		em.getTransaction().commit();
		assertEquals(d,em.find(Rule.class, r.getName()).getDictation());
	}
	@Test
	public void testEquals() {
		Rule r = new Rule("rule4","indice","corps");
		Rule r1 = new Rule("rule4","iniice","jeios");
		assertEquals(r,r1);
	}
}
