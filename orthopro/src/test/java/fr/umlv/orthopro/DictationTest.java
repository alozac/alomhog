package fr.umlv.orthopro;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.umlv.orthopro.entities.Dictation;
import fr.umlv.orthopro.entities.Rule;

/**
 * 
 */ 
public class DictationTest extends JPAHibernateTest{
	@Test
	@BeforeAll
	public static void testPersist() {
		em.getTransaction().begin();
		Rule r = new Rule("rule 0", "indice 0", "corps 0");
		em.persist(r);
		Dictation d = new Dictation("name", "diff", "descr", "audioPath", r);
		em.persist(d);
		em.getTransaction().commit();
		List<Dictation> dicts = em.createNamedQuery("Dictation.getAll",Dictation.class).getResultList();
		assertNotNull(dicts);
		assertEquals(1,dicts.size());
	}
	@Test
	public void testUpdate() {
		em.getTransaction().begin();
		Rule r = new Rule ("rule 1", "indice", "corps");
		em.persist(r);
		em.persist(new Dictation("naame1", "diff", "descr", "audioPath",r));
		Dictation d= em.find(Dictation.class, "name1");
		d.setDescription("newdescr");
		em.getTransaction().commit();
		Dictation d2 = em.find(Dictation.class, d.getName());
		assertEquals(d2.getDescription(), "newdescr");
	}
	@Test
	public void testDelete() {
		em.getTransaction().begin();
		Dictation d = new Dictation("naame","diff","descr","audioPath",new Rule("rule 2", "indice 0", "corps 0"));
		em.persist(d);
		em.remove(d);
		em.getTransaction().commit();
		Dictation d2 = em.find(Dictation.class, d.getName());
		assertNull(d2);
		
	}
	
	@Test
	public void newDictationFieldsNull() {
		em.getTransaction().begin();
		assertAll(
				() -> assertThrows(PersistenceException.class,
								   () -> em.persist(new Dictation("name", null, "descr", "audio", new Rule("rule 3", "indice 0", "corps 0")))),
				() -> assertThrows(PersistenceException.class,
						   		   () -> em.persist(new Dictation("name", "diff", null, "audio", new Rule("rule 4", "indice 0", "corps 0")))),
				() -> assertThrows(PersistenceException.class,
						   		   () -> em.persist(new Dictation("name", "diff", "descr", null, new Rule("rule 5", "indice 0", "corps 0")))),
				() -> assertThrows(PersistenceException.class,
						   		   () -> em.persist(new Dictation("name", "diff", "descr", "audio", null)))
		);
		em.getTransaction().rollback();
	}
	
	@Test
	public void testName() {
		em.getTransaction().begin();
		Rule r = new Rule("rule0","indice","corps");
		Dictation d = new Dictation ("name0", "diff", "descr", "audio",r);
		em.persist(r);
		em.persist(d);
		em.getTransaction().commit();
		assertEquals("name0", em.find(Dictation.class, d.getName()).getName());
	}
	@Test
	public void testDifficulty() {
		em.getTransaction().begin();
		Rule r = new Rule("rule1","indice","corps");
		Dictation d = new Dictation ("name1", "diff", "descr", "audio", r);
		em.persist(r);
		em.persist(d);
		d.setDifficulty("diff2");
		em.merge(d);
		em.getTransaction().commit();
		assertEquals("diff2", em.find(Dictation.class, d.getName()).getDifficulty());
	}
	@Test
	public void testAudio() {
		em.getTransaction().begin();
		Rule r = new Rule("rule2","indice","corps");
		Dictation d = new Dictation ("name2", "diff", "descr", "audio", r);
		em.persist(r);
		em.persist(d);
		d.setAudio("audio2");
		em.merge(d);
		em.getTransaction().commit();
		assertEquals("audio2", em.find(Dictation.class, d.getName()).getAudio());
	}
	@Test
	public void testRule() {
		em.getTransaction().begin();
		Rule r0 = new Rule("rule3","indice","corps");
		Dictation d = new Dictation ("name3", "diff", "descr", "audio", r0);
		Rule r = new Rule("newrule", "indice", "corps");
		em.persist(r0);
		em.persist(r);
		em.persist(d);
		d.setRule(r);
		em.merge(d);
		em.getTransaction().commit();
		assertEquals(r, em.find(Dictation.class, d.getName()).getRule());
	}
	
}
