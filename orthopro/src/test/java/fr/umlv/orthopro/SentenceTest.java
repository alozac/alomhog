package fr.umlv.orthopro;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.umlv.orthopro.entities.Rule;
import fr.umlv.orthopro.entities.Sentence;

public class SentenceTest extends JPAHibernateTest {

	@Test
	public void newSentenceFieldsNull() {
		em.getTransaction().begin();
		assertAll(
				() -> assertThrows(PersistenceException.class,
								   () -> em.persist(new Sentence(null, "correction"))),
				() -> assertThrows(PersistenceException.class,
						   		   () -> em.persist(new Sentence("phrase", null)))
		);
		em.getTransaction().rollback();
	}
	
	@Test
	@BeforeAll
	public static void testPersist() {
		em.getTransaction().begin();
		
		Sentence s = new Sentence("une phrase", "sa correction");
		em.persist(s);
		
		em.getTransaction().commit();

		List<Sentence> sent = em.createNamedQuery("Sentence.getAll", Sentence.class).getResultList();
		assertNotNull(sent);
		assertEquals(1, sent.size());
	}
	

	@Test
	public void testUpdate() {
		em.getTransaction().begin();
		em.persist(new Sentence("phrase test", "correction test"));
		
		Sentence sent = em.createNamedQuery("Sentence.getByWording", Sentence.class)
				   		  .setParameter("wording", "phrase test")
				   		  .getSingleResult();
		
		sent.setCorrection("nouvelle correction");
		
		em.getTransaction().commit();
		
		Sentence sent2 = em.find(Sentence.class, sent.getId());
		
		assertEquals(sent2.getCorrection(), "nouvelle correction");
	}
	
	
	@Test
	public void testDelete() {
		em.getTransaction().begin();
		
		List<Sentence> before = em.createNamedQuery("Sentence.getAll", Sentence.class).getResultList();

		em.remove(before.get(0));
		em.getTransaction().commit();
		
		List<Sentence> after = em.createNamedQuery("Sentence.getAll", Sentence.class).getResultList();
		
		assertTrue(before.size() == after.size()+1);
	}

	
	@Test 
	public void testDeletedOnCascade() {
		em.getTransaction().begin();
		
		Rule rule = new Rule("nom regle cascade", "indice", "corps regle");
		rule.addSentence(new Sentence("phrase test cascade", "correction test"));
		em.persist(rule);
		
		Rule r = em.find(Rule.class, "nom regle cascade");
		Sentence s = r.getSentences().iterator().next();
		
		em.remove(r);
		em.getTransaction().commit();
		
		assertNull(em.find(Sentence.class, s.getId()));
	}
	
	
	@Test
	public void newSentence() {
		Sentence s = new Sentence("L'entreprise a procédé au désamiantage suite a la demande du maître d'ouvrage.",
				"L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage.");
		assertEquals(s, new Sentence("L'entreprise a procédé au désamiantage suite a la demande du maître d'ouvrage.",
				"L'entreprise a procédé au désamiantage suite à la demande du maître d'ouvrage."));
	}



	@Test
	public void testRule() {
		em.getTransaction().begin();
		Sentence s = new Sentence ("lala", "lalala");
		Rule r = new Rule("rule0","indice","corps");
		s.setRule(r);
		em.persist(r);
		em.persist(s);
		em.getTransaction().commit();
		assertEquals(r,em.find(Sentence.class, s.getId()).getRule());
	}
	
	@Test
	public void testWording() {
		em.getTransaction().begin();
		Sentence s = new Sentence("lala","lalala");
		em.persist(s);
		s.setWording("lalalala");
		em.merge(s);
		em.getTransaction().commit();
		assertEquals("lalalala", em.find(Sentence.class, s.getId()).getWording());
	}
	
	@Test
	public void testCorrection() {
		em.getTransaction().begin();
		Sentence s = new Sentence ("lala","lalala");
		em.persist(s);
		s.setCorrection("lalalala");
		em.merge(s);
		em.getTransaction().commit();
		assertEquals("lalalala",em.find(Sentence.class, s.getId()).getCorrection());
	}
}
