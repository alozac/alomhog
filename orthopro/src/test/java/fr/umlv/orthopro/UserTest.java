package fr.umlv.orthopro;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import javax.persistence.PersistenceException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import fr.umlv.orthopro.entities.Dictation;
import fr.umlv.orthopro.entities.Rule;
import fr.umlv.orthopro.entities.Score;
import fr.umlv.orthopro.entities.User;

public class UserTest extends JPAHibernateTest{
	
	@Test
	public void newUserFieldsNull() {
		em.getTransaction().begin();
		assertAll(
				() -> assertThrows(PersistenceException.class,
								   () -> em.persist(new User("pseudo", null)))
		);
		em.getTransaction().rollback();
	}
	
	@Test
	@BeforeAll
	public static void testPersist() {
		em.getTransaction().begin();
		User u = new User("pseudo", "pwd");
		em.persist(u);
		em.getTransaction().commit();
		List<User> users = em.createNamedQuery("User.getAll", User.class).getResultList();
		assertNotNull(users);
		assertEquals(1,users.size());
	}
	
	@Test
	public void testUpdate() {
		em.getTransaction().begin();
		em.persist(new User("pseudo1","pwd"));
		em.getTransaction().commit();
		User u= em.find(User.class, "pseudo1");
		u.setPassword("newpwd");
		User u2 = em.find(User.class, u.getPseudo());
		assertEquals(u2.getPassword(), "newpwd");
	}
	
	@Test
	public void testDelete() {
		em.getTransaction().begin();
		User u = new User("pseudo2", "pwd");
		em.persist(u);
		em.remove(u);
		em.getTransaction().commit();
		User u2 = em.find(User.class, u.getPseudo());
		assertNull(u2);
	}
	
	
	
	
	@Test
	public void addOrReplaceScoreNull() {
		User user = new User("pseudo", "password");
		assertThrows(NullPointerException.class, () -> user.addOrReplaceScore(null, 5));
	}
	
	@Test
	public void addOrReplaceScoreTwice() {
		User user = new User("pseudo", "password");
		Dictation d1 = new Dictation("name", "difficulty", "description", "audio file", new Rule("rule 0", "indice 0", "corps 0"));
		user.addOrReplaceScore(d1, 5);
		user.addOrReplaceScore(d1, 7);
		assertEquals(new Score(5, user, d1), new Score(7, user, d1));
		assertTrue(7 == user.getScore(d1).getScore());
		user.addOrReplaceScore(d1, 3);
		assertTrue(7 == user.getScore(d1).getScore());
	}
	
	
	
	
	
	@Test
	public void isDictationDoneOk() {
		User user = new User("pseudo", "password");
		Dictation d = new Dictation("name", "difficulty", "description", "audio file", new Rule("rule 0", "indice 0", "corps 0"));
		user.addOrReplaceScore(d, 5);
		assertTrue(user.isDictationDone(d));
	}
	
	@Test
	public void isDictationDoneNotOk() {
		User user = new User("pseudo", "password");
		Dictation d = new Dictation("name", "difficulty", "description", "audio file", new Rule("rule 0", "indice 0", "corps 0"));
		assertFalse(user.isDictationDone(d));
	}

	
	@Test
	public void isDictationDoneNull() {
		User user = new User("pseudo", "password");
		assertThrows(NullPointerException.class, () -> user.isDictationDone(null));
	}
	
	
	
	
	

	@Test
	public void addOrReplaceScoreDictationOk() {
		User user = new User("pseudo", "password");
		Dictation d = new Dictation("name", "difficulty", "description", "audio file", new Rule("rule 0", "indice 0", "corps 0"));
		user.addOrReplaceScore(d, 5);
		user.addOrReplaceScore(d, 1);
		assertTrue(user.getScore(d).getScore() == 5);
	}
	
	
	@Test
	public void addOrReplaceScoreDictationNotExists() {
		User user = new User("pseudo", "password");
		Dictation d = new Dictation("name", "difficulty", "description", "audio file", new Rule("rule 0", "indice 0", "corps 0"));
		user.addOrReplaceScore(d, 10);
		assertEquals(user.getScores().size(), 1);
	}
	
	@Test
	public void addOrReplaceScoreDictationNull() {
		User user = new User("pseudo", "password");
		assertThrows(NullPointerException.class, () -> user.addOrReplaceScore(null, 1));
	}
	
	
	
	
	
	@Test
	public void addRuleLearnedNull() {
		User user = new User("pseudo", "password");
		assertThrows(NullPointerException.class, () -> user.addRuleNotLearned(null));
	}
	
	@Test
	public void isRuleLearnedNull() {
		User user = new User("pseudo", "password");
		assertThrows(NullPointerException.class, () -> user.isRuleLearned(null));
	}
	
	
	
	
	
	
	@Test
	public void removeRuleLearnedOk() {
		User user = new User("pseudo", "password");
		Rule r = new Rule("name", "hint", "details");
		user.addRuleNotLearned(r);
		user.removeRuleLearned(r);
		assertTrue(user.getRules().isEmpty());
	}
	
	@Test
	public void removeRuleLearnedNotExists() {
		User user = new User("pseudo", "password");
		assertThrows(IllegalStateException.class, () -> user.removeRuleLearned(new Rule("name", "hint", "details")));
	}
	
	
	
}
