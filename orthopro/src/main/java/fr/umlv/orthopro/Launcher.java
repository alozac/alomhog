package fr.umlv.orthopro;

import fr.umlv.orthopro.server.DBVerticle;
import fr.umlv.orthopro.server.Server;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;

public class Launcher {
	 public static void main(String args[]) {
	       Vertx vertx = Vertx.vertx();
	       vertx.deployVerticle(new DBVerticle(), new DeploymentOptions().setWorker(true));
	       vertx.deployVerticle(new Server());
	   }
}