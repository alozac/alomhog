package fr.umlv.orthopro.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

/**
 * 
 * Reads data from an Excel file
 *
 */
public class XLSFileReader {

	/**
	 * Retrieves data contained in the file of Path <i>filepath</i> into a JsonArray.
	 * Each element of the JsonArray is a JsonObject whose keys are the cells' values of the first row in the file, 
	 * and values the cells' values of a row in the file.
	 * For example, with the following sheet, the JsonArray returned will be 
	 * [{
	 * 		"Title 1": "Row 1,1", 
	 * 		"Title 2": "Row 1,2", 
	 * 		"Title 3": "Row 1,3"
	 * }, 
	 * {
	 * 	 	"Title 1": "Row 2,1", 
	 * 		"Title 2": "Row 2,2", 
	 * 		"Title 3": "Row 2,3"
	 * }]
	 * | Title 1 | Title 2 | Title 3 |
	 * |---------|---------|---------|
	 * | Row 1,1 | Row 1,2 | Row 1,3 |
	 * |---------|---------|---------|
	 * | Row 2,1 | Row 2,2 | Row 2,3 |
	 *  
	 * @param filepath the Path of the Excel file
	 * @return
	 */
	public static JsonArray getContentAsJson(Path filepath) {
		try (Workbook workbook = new XSSFWorkbook(filepath.toFile())) {

			Sheet rulesSheet = workbook.getSheetAt(0);

			Iterator<Row> rowIter = rulesSheet.rowIterator();
			
			ArrayList<String> fstRowTitles = new ArrayList<>();
			Row firstRow = rowIter.next();
			firstRow.forEach( cell -> fstRowTitles.add(cell.getStringCellValue()));
			int fstCellNum  = firstRow.getFirstCellNum();
			int lastCellNum = firstRow.getLastCellNum();

			JsonArray content = new JsonArray();

			rowIter.forEachRemaining(raw -> {
				JsonObject contentLine = new JsonObject();
				for (int i = fstCellNum; i < lastCellNum ; i++) {
					contentLine.put(fstRowTitles.get(i), raw.getCell(i).getStringCellValue());
				}
				content.add(contentLine);
			});

			return content;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e1) {
			e1.printStackTrace();
		}
		return null;
	}

}
