package fr.umlv.orthopro.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@NamedQueries({
	@NamedQuery(
	name = "Sentence.getAll",
	query = "from Sentence"
	),
	@NamedQuery(
	name = "Sentence.getById",
	query = "from Sentence where id_sentence = :id"
	),
	@NamedQuery(
	name = "Sentence.getByWording",
	query = "from Sentence where wording = :wording"
	),
})

@Entity
public class Sentence {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id_sentence")
	private Integer id;
	
	@Column(name="wording", nullable=false)
	private String wording;
	
	@Column(name="correction", nullable=false)
	private String correction;
	
	@ManyToOne
	@JoinColumn(name="id_rule")
	@JsonManagedReference(value="sentences-rule")
	private Rule rule;

	public Sentence() {}
	
	public Sentence(String wording, String correction) {
		this.wording = wording;
		this.correction = correction;
	}

	public Integer getId() {
		return id;
	}

	public String getWording() {
		return wording;
	}

	public void setWording(String wording) {
		this.wording = wording;
	}

	public String getCorrection() {
		return correction;
	}

	public void setCorrection(String correction) {
		this.correction = correction;
	}

	public Rule getRule() {
		return rule;
	}

	public void setRule(Rule rule) {
		this.rule = rule;
	}

	@Override
	public String toString() {
		return "sentence " + id + " :  " + wording + " -> " + correction;
	}
	
	public boolean equals(Object o) {
		if(!(o instanceof Sentence)) {
			return false;
		}
		return ((Sentence)o).getWording().equals(this.getWording()) && ((Sentence)o).getCorrection().equals(this.getCorrection());
	}
	
	@Override
	public int hashCode() {
		return wording.hashCode();
	}
}
