package fr.umlv.orthopro.entities;

import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@NamedQueries({ 
	@NamedQuery(name = "User.getAll", query = "from User"),
	@NamedQuery(name = "User.getByPseudoAndPassword", query = "from User where pseudo=:pseudo and password=:password"),
	@NamedQuery(name = "User.getByPseudo", query = "from User where pseudo=:pseudo")})

@Entity
public class User {

	@Id
	@Column(name = "pseudo", nullable = false)
	private String pseudo;

	@Column(name = "password", nullable = false)
	private String password;

	@ManyToOne
	@JoinColumn(name = "id_role")
	private Role role;

	@ManyToMany()
	@JoinTable(name = "Maitrise", joinColumns = @JoinColumn(name = "pseudo"), inverseJoinColumns = @JoinColumn(name = "id_rule"))
	private Set<Rule> rules = new HashSet<Rule>();

	@OneToMany(mappedBy = "pk.user")
	private Set<Score> scores = new HashSet<Score>();

	public User() {
	}
	
	public User(String pseudo, String password) {
		this.pseudo = pseudo;
		this.password = password;
	}

	public String getPseudo() {
		return pseudo;
	}

	public String getPassword() {
		return password;
	}

	public Role getRole() {
		return role;
	}
	
	public void setRole(Role role) {
		this.role = role;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<Rule> getRules() {
		return rules;
	}
	
	public void setRules(Set<Rule> rules) {
		this.rules = rules;
	}

	public Set<Score> getScores() {
		return scores;
	}

	public void setScores(Set<Score> scores) {
		this.scores = scores;
	}

	/**
	 * Add the score this user obtained for the Dictation <i>dict</i>. 
	 * If this user has already done the dictation, the score is updated only if it is greater than the previous score.
	 * @param dict the Dictation done by this user
	 * @param score the new score obtained by this user
	 */
	public void addOrReplaceScore(Dictation dict, int score) {
		Objects.requireNonNull(dict);
		Score s = new Score(score, this, dict);
		scores.stream().filter(x -> x.equals(s) && x.getScore() < score).forEach(x -> x.setScore(score));
		scores.add(s);
	}

	/**
	 * Returns the score obtained by this user for the Dictation <i>dict</i>.
	 * @param dict the Dictation we want to retrieve the score obtained by this user
	 * @return the score obtained by this user if it exists, null otherwise
	 */
	public Score getScore(Dictation dict) {
		Objects.requireNonNull(dict);
		Optional<Score> opt = scores.stream()
									.filter(x -> x.getPk().getDictation().equals(dict))
									.findAny();
		return opt.isPresent() ? opt.get() : null;
	}

	
	/**
	 * Add a Rule to the set of Rules not already learned by this user.
	 * @param rule the Rule to add to the set of Rules not already learned by this user
	 */
	public void addRuleNotLearned(Rule rule) {
		Objects.requireNonNull(rule);
		rules.add(rule);
	}

	/**
	 * Remove a Rule to the set of Rules not already learned by this user.
	 * @param rule the Rule learned by this user
	 */
	public void removeRuleLearned(Rule r) {
		Objects.requireNonNull(r);
		if(!rules.contains(r)) {
			throw new IllegalStateException();
		}
		rules.remove(r);
	}
	
	public boolean isDictationDone(Dictation d) {
		Objects.requireNonNull(d);
		return this.getScore(d)!=null;
	}
	
	public boolean isRuleLearned(Rule r) {
		Objects.requireNonNull(r);
		if(!this.rules.contains(r)) {
			return true;
		}
		return false;
	}
	@Override
	public int hashCode() {
		return pseudo.hashCode();
	}
	
}
