package fr.umlv.orthopro.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@JsonIgnoreProperties("pk")
public class Score implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Cascade({ CascadeType.PERSIST })
	private ScoreId pk;

	@Column(name = "score")
	private Integer score;

	public Score() {}

	public Score(int s, User u, Dictation dict) {
		this.pk = new ScoreId(dict, u);
		this.score = s;
	}

	public ScoreId getPk() {
		return pk;
	}

	public void setPk(ScoreId pk) {
		this.pk = pk;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Score)) {
			return false;
		}
		return ((Score) o).pk.equals(pk);
	}

	@Override
	public String toString() {
		return "score = " + score;
	}
	
	@Override
	public int hashCode() {
		return pk.hashCode();
	}
}
