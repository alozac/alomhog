package fr.umlv.orthopro.entities;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;
@NamedQueries({
	@NamedQuery(
	name = "Dictation.getAll",
	query = "from Dictation"
	),
	@NamedQuery(
	name = "Dictation.getByName",
	query = "from Dictation where name = :name"
	),
})

@Entity
public class Dictation {

	@Id
	@Column(name="name")
	private String name;
	
	@Column(name="difficulty", nullable=false)
	private String difficulty;
	
	@Column(name="description", nullable=false)
	private String description;

	@Column(name="audio", nullable=false)
	private String audio;

	@OneToOne
	@JsonManagedReference(value="dictation-rule")
	private Rule rule;
	
	public Dictation() {}
	
	public Dictation(String name, String diff, String descr, String audio, Rule rule) {
		this.name = name;
		this.difficulty = diff;
		this.description = descr;
		this.audio = audio;
		this.rule = rule;
	}

	public String getName() {
		return name;
	}

	public String getDifficulty() {
		return difficulty;
	}

	public void setDifficulty(String difficulty) {
		this.difficulty = difficulty;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getAudio() {
		return audio;
	}

	public void setAudio(String audio) {
		this.audio = audio;
	}
	
	public Rule getRule() {
        return rule;
	}
	
	public void setRule(Rule r) {
		this.rule=r;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	public String toString() {
		return name;
	}
	
}
