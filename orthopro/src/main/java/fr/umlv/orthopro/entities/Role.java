package fr.umlv.orthopro.entities;

import javax.persistence.*;

@Entity
public class Role {
	
	@Id
	@Column(name="label_role")
	private String label;
	
	public Role() {}
	
	public Role(String l) {
		label=l;
	}
	
	public String getLabel() {
		return label;
	}
	
	@Override
	public int hashCode() {
		return label.hashCode();
	}
}
