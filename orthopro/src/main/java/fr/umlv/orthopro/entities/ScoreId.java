package fr.umlv.orthopro.entities;

import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Embeddable
public class ScoreId implements java.io.Serializable{

	private static final long serialVersionUID = 1L;

     @ManyToOne
     @JoinColumn(name = "id_dict")
     private Dictation dictation;

     @ManyToOne
     @JoinColumn(name = "pseudo")
     private User user;
     
     public ScoreId() {}
     
     public ScoreId(Dictation dict, User u) {
    	 this.dictation=dict;
    	 this.user=u;
     }
     
     public Dictation getDictation() {
         return dictation;
     }
     
     public void setDictation(Dictation dict) {
         this.dictation = dict;
     }

     public User getUser() {
         return user;
     }
     
     public void setUser(User user) {
             this.user = user;
     }
     
     @Override
     public boolean equals(Object o) {
    	 if(!(o instanceof ScoreId)) {
    		 return false;
    	 }
    	 ScoreId sco = (ScoreId)o;
    	 return sco.user.equals(this.user) && sco.dictation.equals(this.dictation);
     }
     
     @Override
     public int hashCode() {
    	 String hash = dictation.getName() + user.getPseudo();
    	 return hash.hashCode();
     }
}
