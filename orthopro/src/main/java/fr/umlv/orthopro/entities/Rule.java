package fr.umlv.orthopro.entities;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonBackReference;

@NamedQueries({
	@NamedQuery(
	name = "Rule.getAll",
	query = "from Rule"
	),
	@NamedQuery(
	name = "Rule.getByName",
	query = "from Rule where name = :name"
	)
})

@Entity
public class Rule {

	@Id
	@Column(name = "name")
	private String name;

	@Column(name = "hint", nullable = false)
	private String hint;

	@Column(name = "body", nullable = false)
	private String body;

	@OneToMany(mappedBy = "rule")
	@Cascade({ CascadeType.PERSIST, CascadeType.DELETE })
	@JsonBackReference(value = "sentences-rule")
	private Set<Sentence> sentences = new HashSet<Sentence>();

	@OneToOne(mappedBy = "rule")
	@JsonBackReference(value = "dictation-rule")
	private Dictation dictation;

	public Rule() {}

	public Rule(String name, String hint, String body) {
		this.name = name;
		this.hint = hint;
		this.body = body;
	}

	public String getName() {
		return name;
	}

	public String getHint() {
		return hint;
	}

	public void setHint(String hint) {
		this.hint = hint;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public Set<Sentence> getSentences() {
		return sentences;
	}

	public void setSentences(Set<Sentence> sentences) {
		this.sentences = sentences;
	}

	public void addSentence(Sentence sentence) {
		sentences.add(sentence);
		sentence.setRule(this);
	}

	public Dictation getDictation() {
		return dictation;
	}

	public void setDictation(Dictation d) {
		this.dictation = d;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder(" rule : ")
				.append(name).append(" : ")
				.append(hint).append(" -> ")
				.append(body);
		sentences.forEach(s -> sb.append("\n\t").append(s));
		return sb.toString();
	}

	@Override
	public int hashCode() {
		return name.hashCode();
	}

	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Rule)) {
			return false;
		}
		Rule r = (Rule) o;
		return r.name.equals(this.name);
	}

}
