package fr.umlv.orthopro.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Function;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.eventbus.Message;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class Server extends AbstractVerticle {

	@Override
	public void start() {

		HttpServer server = vertx.createHttpServer();
		Router router = Router.router(vertx);

		router.route().handler(BodyHandler.create());

		router.post("/users/login").handler(this::login);
		
		router.post("/users").handler(this::addUser);
		

		router.get("/dictations").handler(this::homepage);

		router.post("/dictations").handler(this::updateDictation);
		
		router.get("/dictations/:name").handler(this::dictationByName);

		router.options("/dictations").handler(this::deleteDictation);
		

		router.get("/rules").handler(this::rulesPage);
		
		
		router.post("/sentences/random").handler(this::training);
		
		router.post("/sentences/verify").handler(this::verifyRule);

		
		router.post("/upload").handler(this::uploadFile);
		
		server.requestHandler(router::accept).listen(9090);
	}
	
	/**
	 * Ask the database to log the user with the credentials contained in the request's body.
	 * @param rc
	 */
	private void login(RoutingContext rc) {
		dBCommunication("login", rc.getBodyAsJson(), rc, 
				new ResponseInfos(200, "application/json", reply -> reply.result().body().toString()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}

	/**
	 * Ask the database to add a new User with informations contained in the request's body.
	 * @param rc
	 */
	private void addUser(RoutingContext rc) {
		dBCommunication("add_user", rc.getBodyAsJson(), rc, 
				new ResponseInfos(201, "application/json", reply -> reply.result().body().toString()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}
	
	/**
	 * The client wants to go to the homepage, so we need all Dictations contained in the database.  
	 * @param rc
	 */
	private void homepage(RoutingContext rc) {
		dBCommunication("get_dictations", rc.request().getParam("pseudo"), rc, 
				new ResponseInfos(200, "text/plain", reply -> (String) reply.result().body()),	 
				new ResponseInfos(400, "text/plain", reply -> "Problème de lecture"));
	}
	
	/**
	 * Ask the database to update (or create) a Dictaction with the informations contained in the request.
	 * @param rc
	 */
	private void updateDictation(RoutingContext rc) {
		JsonObject body = new JsonObject();
		rc.request().params().forEach(ent -> body.put(ent.getKey(), ent.getValue()));
		for (FileUpload f : rc.fileUploads()) {
			try {
				Path path = Paths.get(f.uploadedFileName());
				body.put("audio", Files.readAllBytes(path));
				Files.delete(path);
			} catch (IOException e) {
				buildResponse(rc, 400, "plain/text").end("Error reading file");
			}
		}
		dBCommunication("update_dictation", body, rc, 
				new ResponseInfos(201, "application/json", reply -> reply.result().body().toString()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
		
	}

	/**
	 * Ask the database to retrieve if it exists the Dictation having the name contained in the request.
	 * @param rc
	 */
	private void dictationByName(RoutingContext rc) {	
		dBCommunication("get_dictation", rc.request().getParam("name"), rc, 
				new ResponseInfos(200, "application/json", reply -> ((JsonObject) reply.result().body()).encode()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}
	
	/**
	 * Ask the database to delete the dictation having the name contained in the request.
	 * @param rc
	 */
	private void deleteDictation(RoutingContext rc) {
		dBCommunication("delete_dictation", rc.request().getParam("name"), rc, 
				new ResponseInfos(204, "text/plain", reply -> ""),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}
	
	/**
	 * Ask the database to retrieve all Rules.
	 * @param rc
	 */
	private void rulesPage(RoutingContext rc) {
		dBCommunication("get_rules", rc.request().getParam("pseudo"), rc, 
				new ResponseInfos(200, "application/json", reply -> reply.result().body().toString()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}

	/**
	 * Ask the database to retrieve a random Sentence for the training of the logged User.
	 * @param rc
	 */
	private void training(RoutingContext rc) {
		vertx.eventBus().send("training", rc.getBodyAsString(), reply -> {
			if (reply.succeeded()) {
				String rep = (String) reply.result().body();
				if(rep == null) {
					buildResponse(rc, 204, "text/plain").end();
				}else {
					buildResponse(rc, 200, "text/plain").end((String) reply.result().body());
				}
			} else {
				buildResponse(rc, 400, "text/plain").end();
			}
		});
	}
	
	/**
	 * Ask the database to update if needed the score obtained by the logged User to the Dictation 
	 * having a Rule of name contained in the request.
	 * @param rc
	 */
	private void verifyRule(RoutingContext rc) {
		dBCommunication("verify_rule", rc.getBodyAsJson(), rc, 
				new ResponseInfos(200, "application/json", reply ->reply.result().body().toString()),	 
				new ResponseInfos(400, "text/plain", reply -> ""));
	}

	/**
	 * Ask the database to read the file uploaded by the client and to update the Rules and Sentences 
	 * with the content of this file. 
	 * @param rc
	 */
	private void uploadFile(RoutingContext rc) {
		for (FileUpload f : rc.fileUploads()) {
			dBCommunication("file_upload", f.uploadedFileName(), rc, 
					new ResponseInfos(200, "text/plain", reply -> ""),	 
					new ResponseInfos(400, "text/plain", reply -> "Problème de lecture du fichier"));
		}
	}
	
	/**
	 * Allows to communicate with an other verticle. We send on the event bus the <i>body</i> message 
	 * at the address <i>address</i>, and we handle the response to this message building a response to the client.
	 * The response built for the client will contain the <i>success</i> or <i>fail</i> informations,
	 * depending on the success or fail of the response handled.
	 * @param address the adresse to whiwh send the message
	 * @param body the message to send
	 * @param rc the RoutingContext object to use to respond to the client
	 * @param success the informations of the response to build for the client if the reply received from the event bus is a success
	 * @param fail the informations of the response to build for the client if the reply received from the event bus has failed
	 */
	private void dBCommunication(String address, Object body, RoutingContext rc, ResponseInfos success, ResponseInfos fail) {
		vertx.eventBus().send(address, body, reply ->{
			ResponseInfos resp = reply.succeeded() ? success : fail;
			buildResponse(rc, resp.statusCode, resp.contentType).end(resp.dataBody.apply(reply));
		});
	}
	
	/**
	 * Build the beginning of a response from a status code and a MIME type. 
	 * @param rc the RoutingContext object from which the response is built.
	 * @param statusCode the status code of the response
	 * @param contentType the MIME type of the futur content of the response
	 * @return a HttpServerResponse configured with a status code and a MIME type.
	 */
	private HttpServerResponse buildResponse(RoutingContext rc, int statusCode, String contentType) {
		return rc.response().setStatusCode(statusCode)
				.putHeader("Access-Control-Allow-Origin", "*")
				.putHeader("Content-Type", contentType);
	}

	/**
	 * 
	 * Inner class containing the necessary informations to send a response to a request.
	 * Those informations consist in a status code, a MIME type of the response's body,
	 * and the body itself, which will be the result of a function applied to a reply sent by an other verticle. 
	 *
	 */
	class ResponseInfos {
		private int statusCode;
		private String contentType;
		private Function<AsyncResult<Message<Object>>, String> dataBody;

		private ResponseInfos(int statusCode, String contentType, Function<AsyncResult<Message<Object>>, String> dataBody) {
			this.statusCode = statusCode;
			this.contentType = contentType;
			this.dataBody = dataBody;
		}
	}
}
