package fr.umlv.orthopro.server;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.umlv.orthopro.entities.Dictation;
import fr.umlv.orthopro.entities.Role;
import fr.umlv.orthopro.entities.Rule;
import fr.umlv.orthopro.entities.Sentence;
import fr.umlv.orthopro.entities.User;
import fr.umlv.orthopro.util.XLSFileReader;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

public class DBVerticle extends AbstractVerticle {

	EntityManagerFactory emf;
	EntityManager em;

	@Override
	public void start() throws Exception {
		emf = Persistence.createEntityManagerFactory("OrthoPro");
		em = emf.createEntityManager();

		initBase();

		vertx.eventBus().consumer("login", this::handlerLogin);

		vertx.eventBus().consumer("add_user", this::handlerAddUser);
		

		vertx.eventBus().consumer("get_dictations", this::getDictations);

		vertx.eventBus().consumer("get_dictation", this::getDictationByName);

		vertx.eventBus().consumer("update_dictation", this::updateDictation);
		
		vertx.eventBus().consumer("delete_dictation", this::deleteDictation);
		

		vertx.eventBus().consumer("get_rules", this::getRules);

		
		vertx.eventBus().consumer("training", this::training);
		
		vertx.eventBus().consumer("verify_rule", this::updateScore);
	

		vertx.eventBus().consumer("file_upload", this::handlerUpload);
	}
	
	/**
	 * Attempts to retrieve and send back a User by a pseudo and a password.
	 * @param mess a message containing the User credentials to retrieve
	 */
	private void handlerLogin(Message<JsonObject> mess) {
		JsonObject params = mess.body();
		List<User> u = em.createNamedQuery("User.getByPseudoAndPassword", User.class).setMaxResults(1)
				.setParameter("pseudo", params.getString("pseudo"))
				.setParameter("password", params.getString("password")).getResultList();
		if (u.isEmpty()) {
			mess.fail(400, "Login ou mot de passe invalide");
		} else {
			mess.reply(Json.encode(u.get(0)));
		}
	}

	/**
	 * Create a User with Role "member" in the database with the credentials contained in the message 
	 * if no other User exists with the same pseudo.
	 * @param mess the message containing the credentials of the User to create.
	 */
	private void handlerAddUser(Message<JsonObject> mess) {
		User toAdd = Json.decodeValue(mess.body().toString(), User.class);
		if (em.find(User.class, toAdd.getPseudo()) != null) {
			mess.fail(400, "Pseudo déjà existant");
			return;
		}
		Set<Rule> rules = new HashSet<>(em.createNamedQuery("Rule.getAll", Rule.class).getResultList());
		toAdd.setRules(rules);
		toAdd.setRole(em.find(Role.class, "member"));
		em.getTransaction().begin();
		em.persist(toAdd);
		em.getTransaction().commit();
		mess.reply(Json.encode(toAdd));
	}
	
	/**
	 * Retrieve all Dictations contained in the database, and reply a JsonArray containing those Dictations.
	 * For each of those Dictations, we add a parameter "score" being the score obtained by the logged User for the Dictation,
	 * and a parameter "maxScore" being the maximum score the User can obtain with this Dictation.
	 * @param mess
	 */
	private void getDictations(Message<String> mess) {
		List<Dictation> dict = em.createNamedQuery("Dictation.getAll", Dictation.class).getResultList();
		JsonArray mapped = new JsonArray();
		User u = em.find(User.class, mess.body());
		dict.forEach(d -> mapped
				.add(JsonObject.mapFrom(d).put("score", u.getScore(d) == null ? "" : u.getScore(d).getScore())
				.put("maxScore", d.getRule().getSentences().size())));
		mess.reply(Json.encode(mapped));
	}

	/**
	 * Get a Dictation of name contained in the message, with an additional field "correction" being the correction of the Dictation 
	 * (i.e. the concatenation of the corrections of all sentences applying the Dictation's Rule)
	 * @param mess
	 */
	private void getDictationByName(Message<String> mess) {
		Dictation dict = em.find(Dictation.class, mess.body());
		JsonObject o = JsonObject.mapFrom(dict);
		String corr = dict.getRule().getSentences().stream().map(x -> x.getCorrection())
				.collect(Collectors.joining("\n"));
		o.put("correction", corr);
		mess.reply(o);
	}
	
	/**
	 * Update (or replace if it does not exist) in the database the dictation 
	 * with the informations contained in the message
	 * @param mess
	 */
	private void updateDictation(Message<JsonObject> mess) {
		JsonObject rule = new JsonObject(mess.body().getString("rule"));
		rule.remove("dictation");
		mess.body().put("rule", rule);
		JsonObject jsonUp = mess.body();
		jsonUp.remove("correction");
		Dictation toUpdate = Json.decodeValue(jsonUp.toString(), Dictation.class);
		em.getTransaction().begin();
		Dictation updated = em.merge(toUpdate);
		Rule hasDictUpdated = em.find(Rule.class, rule.getString("name"));
		hasDictUpdated.setDictation(updated);
		em.merge(hasDictUpdated);
		em.getTransaction().commit();
		mess.reply(Json.encode(updated));
	}

	/**
	 * Delete from the database the dictation of name contained in the message.
	 * @param mess
	 */
	private void deleteDictation(Message<String> mess) {
		Dictation d = em.find(Dictation.class, mess.body());
		Rule r = d.getRule();
		r.setDictation(null);
		em.getTransaction().begin();
		em.remove(d);
		em.getTransaction().commit();
	}

	/**
	 * Retrieve all Rules of the database. 
	 * For each of those Rules, we add an additional field "dictation" 
	 * being the name of the dictation applying this Rule, 
	 * or null if no Dictation applies the Rule
	 * @param mess
	 */
	private void getRules(Message<String> mess) {
		List<Rule> rules = em.createNamedQuery("Rule.getAll", Rule.class).getResultList();
		JsonArray mapped = new JsonArray();
		rules.forEach(r -> {
			Dictation dict = r.getDictation();
			mapped.add(JsonObject.mapFrom(r).put("dictation", dict == null ? "null" : dict.getName()));
		});
		mess.reply(Json.encode(mapped));
	}
	
	/**
	 * Compute and return a random Sentence not already learned by the logged User.
	 * Replies nothing if it doesn not exist such a Senence.
	 * @param mess
	 */
	private void training(Message<String> mess) {
		User u = em.find(User.class, mess.body());
		Random rand = new Random();
		List<Sentence> sentences = u.getRules().stream().flatMap(r -> r.getSentences().stream())
				.collect(Collectors.toList());
		if (sentences.size() == 0) {
			mess.reply(null);
		} else {
			Sentence randSen = sentences.get(rand.nextInt(sentences.size()));
			mess.reply(Json.encode(randSen));
		}
	}
	
	/**
	 * Update the score obtained by the logged User after having done the dictation contained in the message.
	 * The list of its not already learned Rules is also updated : if the Dictation is perfectly done (determinated by a boolean in the message), 
	 * then we add the Rule applied by the Dictation to the list of its not learned Rule, else we remove it.
	 * @param mess
	 */
	private void updateScore(Message<JsonObject> mess) {
		JsonObject params = mess.body();
		User u = em.find(User.class, params.getString("user"));
		Rule r = em.find(Rule.class, params.getString("rule"));
		Dictation d = em.find(Dictation.class, params.getString("dictation"));
		u.addOrReplaceScore(d, params.getInteger("score"));
		em.getTransaction().begin();
		if (params.getBoolean("correct")) {
			u.removeRuleLearned(r);
		} else {
			u.addRuleNotLearned(r);
		}
		em.merge(u.getScore(d));
		em.merge(u);
		em.getTransaction().commit();
		mess.reply(Json.encode(u));
	}

	/**
	 * Read the file contained in the message, and update in the database the Rules and Sentences with the file's data.
	 * For each new Rule, we add it to the list of rules not already learned of all Users.
	 * @param mess
	 */
	private void handlerUpload(Message<String> mess) {
		List<Rule> rules = getRulesFromFile(mess.body());
		List<User> users = em.createNamedQuery("User.getAll", User.class).getResultList();
		em.getTransaction().begin();
		try {
			rules.forEach(r -> {
				em.persist(r);
				users.forEach(u -> {
					u.addRuleNotLearned(r);
					em.merge(u);
				});
			});
		} catch (EntityExistsException e) {}
		if (!em.getTransaction().getRollbackOnly()) {
			em.getTransaction().commit();
		} else {
			em.getTransaction().rollback();
		}
		mess.reply(null);
	}

	/**
	 * Computes a List of Rules with the data of the file of path <i>filePath</i>.
	 * The Sentences described in the File are added to their respective computed Rule.
	 * @param filePath the path of the file to read
	 * @return a List of Rules described in the file.
	 */
	private List<Rule> getRulesFromFile(String filePath) {
		Path path = Paths.get(filePath);
		JsonArray content = XLSFileReader.getContentAsJson(path);
		deleteFile(path);

		HashMap<String, Rule> rules = new HashMap<>();
		content.forEach(o -> {
			JsonObject obj = (JsonObject) o;
			Rule r = new Rule(obj.getString("Type de difficulté"), obj.getString("Indice"),
					obj.getString("Comment ne pas se tromper"));
			Sentence s = new Sentence(obj.getString("Phrase"), obj.getString("Correction"));

			rules.getOrDefault(r.getName(), r).addSentence(s);
			rules.putIfAbsent(r.getName(), r);
		});
		
		return rules.values().stream().collect(Collectors.toList());
	}
	
	/**
	 * Delete the file stocked in the file_uploads directory once this file has been read.
	 * @param path the Path of the file
	 */
	private void deleteFile(Path path) {
		try {
			Files.delete(path);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Populates the database with some datas.
	 */
	private void initBase() {
		em.getTransaction().begin();
		
		// On ajoute les rôles "membre" et "admin"
		em.persist(new Role("member"));
		em.persist(new Role("admin"));
		
		// Un utilisateur administrateur
		User u = new User("marion", "hoguet");
		u.setRole(em.find(Role.class, "admin"));
		try {
			
			// On crée ici 2 règles, et 2 dictées faisant appliquer ces règles
			Rule r1 = new Rule("Homophone quand/quant", "Remplacer par 'Lorsque', 'En ce qui concerne'", "'Quand' s'utilise pour une indication de lieu.");
			Rule r2 = new Rule("Homophones 'censé' et 'sensé'", "Un rapport avec le bon sens?", "'censé' signifie 'supposé'. On est censé faire quelque chose signifie qu’on est supposé faire quelque chose. Il suffit donc de replacer le mot par 'supposé' pour voir si votre phrase garde bien le sens initial. Censé est toujours suivi d’un infinitif.");
			Dictation d = new Dictation("Dictée 1", "Facile", "Dans cette dictée, vous tenterez de bien appliquer la règle des homophones quand et quant.",
					Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get("audio_files/homonymes_quand_quant.mp3"))), r1);
			Dictation d2 = new Dictation("Dictée 2", "Moyenne", "Censé ou sensé, il faut choisir!",
					Base64.getEncoder().encodeToString(Files.readAllBytes(Paths.get("audio_files/homonymes_cense_sense.mp3"))), r2);
			
			r1.setDictation(d);
			r2.setDictation(d2);
			
			// Chacune des deux règles possède une phrase d'entraînement
			r1.addSentence(new Sentence("Quand à lui, je ne savais pas quand il viendrait.", "Quant à lui, je ne savais pas quand il viendrait."));
			r1.addSentence(new Sentence("Quand je suis arrivé, il y en avait quant même beaucoup.", "Quand je suis arrivé, il y en avait quand même beaucoup."));
			r2.addSentence(new Sentence("Je suis censé aller à Paris, mon frère est sensé être déjà arrivé.", "Je suis censé aller à Paris, mon frère est censé être déjà arrivé."));
			r2.addSentence(new Sentence("Il était censé travailler hier.", "Il était censé travailler hier."));

			// On indique que l'utilisateur ne connaît pas la première règle (et connaît la seconde)
			u.addRuleNotLearned(r1);
			
			em.persist(r1);
			em.persist(r2);
			em.persist(d);
			em.persist(d2);
			em.persist(u);
			em.getTransaction().commit();
		} catch (IOException e) {
			System.out.println("Erreur initialisation de la base");
		}
		System.out.println("base created");
	}
}