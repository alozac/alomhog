import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Auth from '@/components/auth/Auth'
import Upload from '@/components/Upload'
import Dictation from '@/components/Dictation'
import Training from '@/components/Training'
import Rules from '@/components/Rules'
import Revision from '@/components/Revision'
import DictationForm from '@/components/DictationForm'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/login',
      name: 'Auth',
      component: Auth
    },
    {
      path: '/rules',
      name: 'Rules',
      component: Rules
    },
    {
      path: '/dictations/:name/rules',
      name: 'Revision',
      component: Revision
    },
    {
      path: '/upload',
      name: 'Upload',
      component: Upload
    },
    {
      path: '/dictations/:name',
      name: 'Dictation',
      component: Dictation
    },
    {
      path: '/dictations_manager/:name',
      name: 'DictationForm',
      component: DictationForm
    },
    {
      path: '/training',
      name: 'Training',
      component: Training
    },
    {
      path: '/*',
      redirect: { name: 'Auth' }
    }
  ]
})
