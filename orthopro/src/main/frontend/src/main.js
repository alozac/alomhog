// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Navbar from './components/Navbar.vue'
import BootstrapVue from 'bootstrap-vue'
import VueSession from 'vue-session'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VeeValidate, { Validator } from 'vee-validate'
import messages from 'vee-validate/dist/locale/fr'

Validator.localize('fr', messages)
const config = {
  errorBagName: 'errors', // change if property conflicts
  fieldsBagName: 'inputs',
  locale: 'fr',
  inject: true
}

Vue.use(VeeValidate, config)
Vue.use(BootstrapVue)
Vue.use(VueSession)

Vue.component('navbar', Navbar)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
