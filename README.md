# Présentation:

Application d'aide en orthographe réalisée dans le cadre d'un projet de Java en M1.
L'utilisateur peut s'entraîner sur des phrases ciblées par difficulté, d'effectuer des dictées, ainsi que de réviser les principales règles d'orthographe, grammaire, conjugaison, etc.
Voir SPEC.md pour plus de détails.

# Installation:


## Démarrage du serveur:

- importer les fichiers en tant que projet dans Eclipse
- "Run" Launcher.java

## Lancement de vuejs:

- se positionner dans le dossier /orthopro/src/main/frontend
- npm install pour installer les dépendances
- npm run dev pour lancer vuejs

L'application est disponible à l'adresse "http://localhost:8080".
Un compte administrateur existe déjà, avec le pseudo "admin" et le mot de passe "admin"