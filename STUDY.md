Ce document est une étude comparée des frameworks Spring 5 et Vertx 3.5

# Installation

L'installation des deux frameworks consiste à rajouter les dépendances nécéssaires dans le fichier de configuration de Maven pom.xml. En ce qui concerne Vert.x, nous n'avons eu besoin que de deux dépendances (vert.x-core et vert.x-web) que nous avons facilement trouvées.
Par contre, Spring nous a posé plus de problèmes : nous ne savions pas quelles dépendances prendre, et nous avons eu l'impression qu'il en fallait beaucoup plus que pour Vert.x. En effet, lors de l'implémentation de Spring, il y avait toujours des classes qu'Eclipse ne trouvait pas dans les dépendances Maven, et nous avons dû chercher à plusieurs reprises la dépendance spécifique à une classe en particulier.
Cette difficulté est peut-être la conséquence de recherches incomplètes ou d'une mauvaise organisation de notre part, que nous n'avons pas compris, bien suivi ou trouvé les bonnes explications mais nous avons trouvé l'installation de Vert.x bien plus accessible.


# Implémentation

## Recherches et apprentissage

Avant de commencer l'implémentation d'un prototype de notre application avec les deux frameworks, nous avons voulu nous comprendre l'utilité et le fonctionnement de chacun d'entre eux, et nous faire un premier avis global via différents tutoriels et exemples.
Concernant nos recherches sur Vert.x, nous n'avons pas eu de problèmes particuliers, tous les exemples que nous avons trouvés étaient très similaires. Mais nous avons eu plus de mal à trouver des informations concernant Spring 5 : il existe un très grand nombre de guides et tutoriels sur Spring, mais lorsque nous avons recoupé les informations trouvées, nous nous sommes rendu compte que les différents exemples pouvaient être très différents les uns des autres. La version 5 de Spring semble avoir apporté un nombre assez conséquent de changements comparé aux versions précédentes, et trouver des guides et exemples sur Spring 5 a été assez laborieux pour nous.

## Implémentation des prototypes

En terme d'implémentation, nous avons trouvé celle de Spring plus complexe que celle de Vert.x : le fait de suivre un modèle MVC a ralenti notre compréhension de l'interaction des classes les unes avec les autres. Nous avons eu du mal à comprendre le rôle de chaque "couches", et nous nous sommes un peu perdus dans le nombre de classes à créer : les classes du modèle (User, Rule, etc.), les interfaces et leur implémentations de la couche DAO pour chacune de ses classes (UserRepository+UserRepositoryImpl, RuleRepository+RuleRepositoryImpl, etc), et les contrôleurs de chaque classe du modèle (UserController, RuleController, etc.). 


```	     
@Repository
public interface UserRepository {
	User save(User user);
}
	
@RestController
public class UserController

	private final PersonRepository repository;

   	public Mono<ServerResponse> createUser(ServerRequest request) { 
		Mono<User> user = request.bodyToMono(User.class);
		return ServerResponse.ok()
		 					 .build(repository.saveUser(User));
	}
}
```

     
Nous avons ci-dessus seulement une partie du contrôleur et de l'interface décrivant la couche DAO pour la ressource User.

Avec Vert.x, nous avons eu l'impression que pour pouvoir implémenter nos endpoints, il suffisait d'assimiler le concept de Verticles, comment lancer un serveur et gérer les différentes routes.


```
public class Server extends AbstractVerticle {

	@Override
	public void start() {
	
		HttpServer server = vertx.createHttpServer();
		Router router = Router.router(vertx);
			
		// Avec addUser de signature void addUser(RoutingContext)
		router.post("/users")
			  .handler(this::addUser);
				  
		server.requestHandler(router::accept)
			  .listen(8080);
	}
}
```


Comme on le voit ci-dessus, tous les principaux mécanismes peuvent se regrouper dans un Verticle, et pour chaque route, nous avons juste besoin de spécifier l'URL concernée et la méthode prenant en charge ce endpoint.

Le principe de fonctionnement de Vert.x nous a donc paru plus facile à comprendre en peu de temps. De plus, la manière dont les différents Verticles pouvaient communiquer grâce à l'EventBus nous a paru très instinctive et facile à prendre en main, et nous avons pu facilement séparer la gestion des différentes routes avec celles des requêtes à la base de données, conservant ainsi en partie un modèle de séparation des tâches comme avec Spring.


```
// Server
public void addUser(RoutingContext rc) {
	JsonObject body = rc.getBodyAsJson();
	if (!(body.containsKey("pseudo") && body.containsKey("password"))) {
		rc.fail(400);
		return;
	}
	vertx.eventBus()
		 .send("add_user", body, reply -> {
			if (reply.succeeded()) {
				rc.response()
			  	  .setStatusCode(201)
				  .end("<html>utilisateur créé</html>");
			} else {
				rc.fail(400);
			}
		});
}

//DBVerticle
public void start() throws Exception {
		...
		vertx.eventBus().consumer("add_user", this::handlerAddUser);
}	
private void handlerAddUser(Message<JsonObject> mess) {
	User toAdd = Json.decodeValue(mess.body().toString(), User.class);
	em.getTransaction().begin();
	em.persist(toAdd);
	em.getTransaction().commit();
	mess.reply(null);
}
```

Un autre avantage que nous trouvons à Vert.x est le fait qu'il y a peu de nouveaux objets propres au framework à manipuler, alors que Spring utilise différentes annotations, dont il existe des objets équivalents depuis Spring 5 (comme RouterFunction équivalent à l'annotation @RequestMapping), et introduit des objets que nous n'avons jamais eu l'occasion de manipuler, en particulier Mono< T > et Flux< T >, même si ceux-ci se manipulent comme des Stream.

# Conclusion

Nous avons donc choisi d'utiliser Vert.x pour la suite du projet, car il s'agit à notre sens d'un framework plus facile à appréhender et à prendre en main que Spring. Nous avons trouvé ce dernier plus "verbeux" et moins intuitif que Vert.x. De plus, lors de nos recherches, nous avons eu l'impression que Spring 5 était très différent et introduisait de nombreux changements par rapport aux versions antérieures, et les guides et documentations que nous avons trouvés sur Spring 5 nécessitaient de bien connaître et d'être à l'aise avec les versions précédentes. Peut-être qu'en y consacrant plus de temps nous aurions au final choisi d'utiliser Spring 5, mais nous nous sentons davantage à l'aise avec Vert.x, dont on trouve des guides et exemples plus clairs et complets que Spring 5.
