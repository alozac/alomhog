# Projet de Java avancé
------------------------------
## Présentation
Notre projet consiste à implémenter une application dédiée à aider les utilisateurs à améliorer leur orthographe. 
Pour ce faire, l'application doit permettre à l'utilisateur de s'entraîner sur des phrases ciblées par difficulté, d'effectuer des dictées, ainsi que de réviser les principales règles d'orthographe, grammaire, conjugaison, etc.  

Le déroulement de ce que l'on appellera ici une dictée doit être le suivant : l'utilisateur écoute un enregistrement audio stocké dans un fichier .mp3, et il écrit ce qu'il entend dans un emplacement prévu à cet effet.
On ne souhaite pas ici seulement donner une correction de ce qu'il a fait et lui indiquer ses erreurs. En effet, on veut pouvoir lui donner des indications (s'il le souhaite) quant aux pièges dans lesquels il pourrait tomber. 
L'intérêt de ces indices est de permettre à l'utilisateur de progresser de façon plus autonome, on ne veut pas qu'il s'en remette seulement à une correction, mais qu'il réfléchisse bien à ce qu'il écrit, qu'il prenne de bonnes habitudes.
Nous souhaitons également que l'utilisateur ne soit pas noyé dans une liste interminable d'indices concernant des règles qu'il connaît déjà : ne seront donc affichés que les règles qu'il ne maîtrise pas encore.

Lorsque l'utilisateur pense avoir terminé une dictée, il doit pouvoir accès à la correction de la dictée, où il pourra voir s'il a bien respecté les règles concernées par la dictée.

Avant d'effectuer une dictée, il aura tout d'abord accès à une page listant toutes les règles qu'il doit assimiler, et pourra également tester ce qu'il a appris avec des phrases d'entraînement ciblées par difficulté, chaque phrase ne contenant qu'une seule règle à bien appliquer. 
Lors de ces entraînements, il pourra comme pour les dictées demander l'indice concernant la règle présente dans la phrase. De plus, si un des indices ne l'aide pas suffisamment, il aura la possibilité de de voir la règle complète. 
Par exemple, si la phrase d'entraînement est "L'entreprise a procédé au désamiantage suite a la demande du maître d'ouvrage", qui fait appliquer la règle d'homophonie à/a, l'indice sera "avait ou non",  et s'il ne se souvient plus comment appliquer cette règle, l'application lui rappellera : "Penser à essayer "avait" dès qu'on s'apprête à écrire "a" tout seul ! Si "avait" peut remplacer a dans la phrase , pas d'accent. Sinon : un accent".

L'ensemble des règles orthographiques avec les phrases d'entraînement associées devront être stockées dans un fichier Excel, qui pourra être modifié à tout moment sans perturber le fonctionnement de l'application. La personne en charge de ce fichier ne doit pas avoir besoin de connaissances en informatique ni de connaître le fonctionnement interne de l'application pour effectuer une modification ou un ajout dans le fichier.
 
## Fonctionnalités

#### Compte utilisateur:
Notre application permettra à l'utilisateur de se créer un compte. Ainsi, nous pourrons enregistrer les règles d'orthographe qu'il maîtrise ou non (voir la partie sur l'écran des règles), ainsi que les phrases d'entraînement et les dictées qu'il a déjà faites ou non. 

Avoir un système de comptes utilisateur permettra également d'avoir un compte administrateur, qui aura la possibilité de mettre à jour l'application, par exemple en modifiant le fichier contenant la liste des règles et phrases d'entraînement, ou en rajoutant, modifiant ou supprimant des dictées et des corrections.

A son arrivée sur l'application, il sera demandé à l'utilisateur un pseudo et un mot de passe pour se connecter à son compte. S'il ne possède pas de compte, il aura la possibilité d'en créer un.

![imgConn](img/connexion.png)


#### Ecran d'accueil:
##### Utilisateur:

Une fois connecté, l'utilisateur arrivera sur l'écran d'accueil de son compte, à partir duquel il aura la possibilité d'effectuer de nouvelles dictées, de s'entraîner, ainsi que de réviser l'ensemble des règles d'orthographe qu'il doit savoir appliquer.


Dans la liste de dictées, une dictée sera affichée de la manière suivante:

| Difficulté | Nom | Description | Taux de réussite |
|------------|-------|---------------|--------------------|

La description sera seulement une petite phrase à destination de l'utilisateur l'informant du contenu de la dictée (sujet de la dictée, principales règles à appliquer par exemple).

![ecranAccueil](img/accueil.jpg)

A côté de chaque dictée se trouvera une "fiche de révision", c'est-à-dire une page détaillant la règle à connaître (qu'il maîtrise ou non, une fiche de révision est propre à une dictée) avant d'effectuer la dictée.
A son arrivée sur cette page, l'utilisateur ne verra que la liste des règles à réviser. S'il souhaite connaître le détail d'une règle en particulier, il pourra cliquer dessus, ce qui déroulera la règle dans son entièreté.
Lorsque l'utilisateur considère qu'il a suffisamment révisé, il peut cliquer sur le bouton "Je suis prêt !" afin d'effectuer la dictée.

![ecranRevision](img/fiche_revision.jpg)

Le bouton "Déconnexion" déconnectera l'utilisateur et le redirigera vers la page de connexion.
###### Administrateur:

Un compte administrateur pourra utiliser les mêmes fonctionnalités qu'un compte classique, mais aura certains droits supplémentaires, accessibles via plusieurs boutons qu'il trouvera sur la page d'accueil, au-dessus de la liste des dictées.

* Rajouter une dictée: lorsqu'il clique sur le bouton "Nouvelle dictée", l'administrateur est emmené vers une nouvelle page sur laquelle se trouve un formulaire. Ce formulaire demandera les caractéristiques de la nouvelle dictée, c'est à dire:
	* Le nom
	* La difficulté
	* La description (renseignant quelles règles sont appliquées dans la dictée par exemple)
	* La piste audio de la dictée (sous forme d'un fichier .wav à joindre)
	* La correction
	* La liste de toutes les règles où l'administrateur pourra cocher celles qui interviennent dans la dictée. Cette étape nous permettra de construire les fiches de révisions, ainsi que de suggérer cette dictée à l'utilisateur quand il révise une règle en particulier (voir plus bas).
![imgAjout](img/ajouter_dictee.jpg)
* Modifier/supprimer une dictée: à côté de chaque dictée se trouveront deux boutons : un pour supprimer la dictée, l'autre pour modifier la dictée.
	* Si l'on souhaite supprimer, un message de confirmation sous forme de pop-up apparaît. Si l'administrateur confirme la suppression, la dictée n'apparaîtra plus dans la liste.
	* Si l'on souhaite modifier, l'administrateur est emmené vers le même formulaire que précédemment, sauf que celui-ci sera déjà pré-rempli avec les caractéristiques de la dictée.
* Modifier la liste des règles: l'administrateur pourra joindre directement un nouveau fichier Excel contenant les règles et les phrases d'entraînement via le bouton "Joindre un fichier".

![imgAdmin](img/admin.jpg)

#### Ecran de dictée:

Pour effectuer une dictée, l'utilisateur doit cliquer sur celle-ci sur la page d'accueil (ou bien via la fiche de révision de celle-ci). Il peut bien sûr effectuer une dictée autant de fois qu'il le souhaite. Le score affiché sur l'écran d'accueil pour une dictée donnée correspond au score le plus élevé obtenu sur cette dictée. 

L'écran de dictée sera divisé en 3 parties :

* Le champ de texte lui permettant d'écrire ce qu'il entend
* Les indices sur la règle qui intervient dans la dictée. La correction s'affichera au même endroit
* Le bilan de la dictée, c'est-à-dire les règles qu'il doit réviser au vu des fautes qu'il a faites, et les avertissements à propos des règles qu'il était censé maîtriser mais qu'il n'a pas bien appliquer ici

Au dessus du champ de saisie se trouvera la piste audio que l'utilisateur manipulera à sa guise pour écouter la dictée. Il pourra à tout moment revenir en arrière pour réécouter un passage, mettre sur pause, etc.

Sous le champ de texte se trouveront deux boutons : 

* Le premier pour afficher/cacher les indices, qui seront des rappels simples de la règle intervenant dans la dictée. 
Nous n'afficherons pas ici le détail de ces règles, seulement des indices : les détails ne seront accessibles qu'à partir de l'écran d'accueil (via le lien "Liste des règles" sur le menu, ou via les fiches de révision).
* Le second pour afficher la correction. Si l'utilisateur clique sur le bouton "Correction", il ne pourra plus modifier ce qu'il a saisi, et se verra attribuer un taux de réussite. Nous mettrons en valeur (en rouge sur ce qu'il a écrit, en vert la correction) les fautes qu'il a faites afin qu'il puisse comparer facilement ce qu'il a écrit avec la correction.

Les indices et la correction seront tous deux affichés à droite du champ de saisie, et ne pourront jamais être affichés ensemble: lorsque l'utilisateur demande la correction, celle-ci s'affichera "par dessus" les indices s'il les avait demandé.

![ecranDictee](img/dictee.jpg)

#### Ecran d'entraînement:
L'utilisateur pourra s'entraîner via le lien "Entraînement" du menu de la page d'accueil. Un entraînement se présentera sous la forme de phrases contenant chacune une seule erreur correspondant à une règle en particulier. L'utilisateur devra réécrire chacune de ces phrases une à une en appliquant correctement la règle.
Le champ de saisie de sa réponse se trouvera sous la phrase, et sous ce champ de saisie se trouveront trois boutons :

* Un bouton "Indice" qui affichera un indice concernant la règle à appliquer dans cette phrase si l'utilisateur ne voit pas où se trouve l'erreur
* Un bouton "Rappel" qui affichera la règle complète. L'utilisateur utilisera ce bouton si l'indice ne lui suffit pas, qu'il ne voit toujours pas où se trouve l'erreur ou s'il ne se souvient plus comment appliquer la règle
* Un bouton "Correction" qui lui permettra de savoir si sa réponse est correcte ou non. Si c'est le cas, un message de félicitation apparaît, sinon, la bonne réponse s'affichera suivie du détail de la règle qu'il fallait appliquer

A chaque fois qu'il demandera la correction de la phrase d'entraînement actuelle, il débloquera l'accès à un bouton "Suivant" situé en bas à droite de la page qui le dirigera vers une autre phrase d'entraînement.

Les phrases d'entraînement proposées à l'utilisateur seront uniquement des phrases faisant appliquer une règle que l'utilisateur ne maîtrise pas encore, nous ne le ferons pas s'entraîner sur des règles qu'il sait appliquer correctement.

![imgEntr](img/entrainement.jpg)

#### Ecran des règles:
Cet écran ne sera accessible qu'à partir de l'écran d'accueil via le lien "Liste de règles" sur le menu. Il listera toutes les règles à connaître (c'est-à-dire toutes les règles renseignées par l'administrateur dans le fichier Excel), celles que l'utilisateur maîtrise (affichées en vert) situées en-dessous de celles qu'il ne maîtrise pas encore, ceci pour qu'il voit directement les règles qu'il doit apprendre (comme les dictées réussies à 100% sur l'écran d'accueil, les règles déjà maîtrisées sont moins intéressantes pour l'utilisateur que les autres). Quand l'utilisateur clique sur une règle, le détail de celle-ci apparaît, ainsi que des exemples de phrases utilisant cette règle, et une liste de dictées qu'il n'a pas encore totalement réussi faisant intervenir cette règle, et qui lui permettront de s'entraîner.

Un utilisateur maîtrisera une règle s'il l'applique de la bonne manière sur une dictée.

Si l'utilisateur effectue une faute sur une règle qu'il maîtrisait, on considèrera qu'il a oublié la règle, et celle-ci n'apparaîtra alors plus en vert dans la liste des règles. 
Un utilisateur ne pourra pas manuellement changer l'état d'une règle non maîtrisée en maîtrisée (ou inversement): la seule manière de maîtriser une règle est de l'appliquer correctement sur la dictée demandant de l'appliquer.

![imgRegle](img/liste_regles.jpg)