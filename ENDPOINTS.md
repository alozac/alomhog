
### Ressource Règle :

####  GET \rules:

 Liste l'ensemble des règles. Pour chaque règle, en plus des informations générales la concernant (nom, indice et corps), la réponse indiquera si l'utilisateur maîtrise la règle ou non.
 
+  Arguments : aucun argument
 
+ Réponses
	+ 200 OK: description
	
``` 
{
	"rules" : [
		{
			"name" : "nom regle 1",
			"hint" : "indice regle 1",
			"body" : "corps regle 1",
			"master" : true
		}
		,
		{
			"name" : "nom regle 2",
			...
		},
		... 
	]
}
```

#### POST \upload

Ajoute un nouveau fichier de règles (et remplace le précédent).

+ Arguments : 

| Argument | Description | type |
| --------------- | ------------ | ------------ |
| file | Le fichier à remplacer | File |


+ Réponses possibles :

  + 200 OK
  + 404 Not Found : Le fichier n'existe pas

### Ressource Utilisateur:

#### POST \users

Ajoute un utilisateur, dont les informations sont transmises dans le corps de la requête.

+ Arguments :

| Argument | Description | type | 
| --------------- | ------------ | ------------ |
| body | Utilisateur à ajouter | object |


+ Réponses possibles : 
	  + 201 Created
	  + 400 BAD REQUEST : Données mal structurées, ou pseudo déjà existant


#### GET \users\login{?pseudo, password}

Connecte l'utilisateur avec les identifiants pseudo/password.

+ Arguments :

| Argument | Description | type | 
| --------------- | ------------ | ------------ |
| pseudo | Le pseudo de l'utilisateur | string |
| password | Le mot de passe de l'utilisateur | string | 


+ Réponses :
  + 200 OK  
  + 400 Bad Request : pseudo ou mot de passe invalide

#### GET \users\logout

Déconnecte l'utilisateur courant.

+ Arguments : pas d'arguments.
+  Réponses possibles : 
  + 200 OK
  
### Ressource Dictée :

#### GET \dictations

Liste toutes les dictées. La réponse renseignera pour chaque dictée le score obtenu par l'utilisateur courant.

+ Arguments : pas d'arguments.
+  Réponses possibles : 
  + 200 OK
  
  ```
  {
	    "dictations": [
	      {
	        "name":"name1",
	        "difficulty":"diff1",
	        "description":"descr1",
	        "rules": [
	          {
	            "name" : "nom regle 1",
	            "hint" : "indice regle 1",
	            "body" : "corps regle 1"
	          },
	          ...
	        ],
	        "score":"score1",
	      },
	      ...
	    ]
  }
  ```
  
####  GET \dictations\\{name}

Renvoie le nom et le fichier audio de la dictée de nom "name" que l'utilisateur souhaite effectuer.

+ Arguments: 

| Argument | Description | type |
| --------------- | ------------ | ------------ | 
| name | Le nom de la dictée à effectuer | String |


+ Réponses possibles
  + 200 OK 
  
	  ``` 
	  {
	    "dictation": 
	      {
	          "name":"name1",
	          "audiopath":"audiopath1"
	      }
	  }
	``` 
   + 404 NOT FOUND : dictée de nom "name" non trouvée

####  GET \dictations\\{name}\rules:

Permet d'obtenir la liste des règles intervenant dans la dictée de nom "name". Pour chaque, règle, la réponse devra spécifier si l'utilisateur maîtrise ou non la règle.

+ Arguments : 

| Argument | Description | Type | 
| -------- | ----------- | ---- | 
| name | Nom de la dictée dont on souhaite extraire les règles | string |


+ Réponses : 
	+ 200 OK : 
	
	```
	{
		"rules" : [
			{
				"name" : "nom regle n°1",
				"hint" : "indice regle n°1",
				"body" : "corps regle n°1",
				"master" : true
			},
			{
				"name" : "nom regle n°2",
				"hint" : "indice regle n°2",
				"body" : "corps regle n°2",
				"master" : false
			},
			...
		]
	}	
	```
 	+ 404 Not Found : Dictée  de nom "name" non trouvée
 	
#### GET \dictations\\{name}\correction

Permet d'obtenir la correction de la dictée de nom "name", ainsi que le score obtenu par l'utilisateur et la liste des règles qu'il n'a pas su appliquer dans cette dictée. La réponse fournie par l'utilisateur sera envoyée dans le corps de la requête.

+ Arguments : 

| Argument | Description | Type | 
| -------- | ----------- | ---- | 
| name | Nom de la dictée effectuée par l'utilisateur | string |
| body | Réponse proposée par l'utilisateur | string | 


+ Réponses : 
	+ 200 OK : 
	
	```
{
		"correction" : "correction de la dictée de nom "name"",
		"score" : 9,
		"rules" : [
			{
				"name" : "nom regle a reviser n°1",
				"hint" : "indice regle a reviser n°1",
				"body" : "corps regle a reviser n°1"
			},
			{
				"name" : "nom regle a reviser n°2",
				"hint" : "indice regle a reviser n°2",
				"body" : "corps regle a reviser n°2"
			},
			...
		]
}	
	```

 	+ 404 Not Found : Dictée  de nom "name" non trouvée
	+ 400 Bad Request : Format de la requête non valide

#### POST \dictations

Crée une nouvelle dictée, ou modifie les informations d'une dictée si le nom de la dictée envoyée dans le corps de la requête existe déjà.

+ Arguments : 

| Argument | Description | Type | Requis |
| -------- | ----------- | ---- | ------ |
| body | Dictée ajoutée ou modifiée | object | Oui |


+ Réponses:
	+ 200 OK : Opération réussie
	+ 400 Bad Request : Dictée fournie non valide

#### DELETE \dictations\\{name}

Supprime la dictée de nom "name".

+ Arguments : 

| Argument | Description | Type | 
| -------- | ----------- | ---- |
| name | Nom de la dictée à supprimer | string |


+ Réponses :
	+ 200 OK : Opération réussie
	+ 404 Not Found : La dictée de nom "name" n'a pas été trouvée 

### Ressource Phrase d'entraînement: 

#### GET \sentences\random

Récupère une phrase d'entraînement aléatoire choisie parmi celles faisant appliquer une règle non encore maîtrisée par l'utilisateur.

+ Arguments : aucun argument
+ Réponses:
	+ 200 OK:
	
``` 
{
	"sentence" :
		{
			"id" : 42,
			"wording" : "énoncé de la phrase d'entraînement",
			"correction" : "correction de la phrase ",
			"rule" : 
				{
					"name" : "nom règle associée à la phrase",
					"hint" : "indice de la règle",
					"body" : "corps de la règle"
				}
		}
}
```
